<!--
You have 5 minutes to talk about a software development topic or challenge you are facing.
Please focus on specific topics related to software development, such as specific approaches, difficulties you are facing, or unsolved problems.
-->

<!-- ============ -->
# Todo's

*Complete/Adapt the following points*

- [ ] Complete your issue title `[Pitch] Your Title`
- [ ] Adapt the title `# Pitch - 'Your Title'`
- [ ] Add an author under **Author**
- [ ] Summarize your topic in 1 to 5 sentences in **Topic**.
- [ ] Add some keywords for indexing in **Keywords**.
- [ ] Provide references to your projects/ideas, only if you like to do so.

*After your Discussion*

- [ ] Document your discussion results (foto/text)
- [ ] Add the names of the discussion sparring partners

*Complete by organization*

- [ ] Add a date
- [ ] Add additional information (venue,...)

<!-- Do not change -->
/label Topic
/label Pres::Pitch
/label Type::ownSW


<!-- ============ -->
# Pitch - 'Your Title'

<!------------->
<!-- SECTION -->
<!------------->
## Author

<!-- Please specify a list of authors. -->


<!------------->
<!-- SECTION -->
<!------------->
## Topic

<!-- Please focus on specific topics related to software development, such as specific approaches, difficulties you are facing, or unsolved problems. -->


<!------------->
<!-- SECTION -->
<!------------->
## What is one of the challanges/questions you face during your SE project?

<!-- 
- Are you stuck?
- Where do you need support?
- If you share a tool, why do you use it in the first place (motivation)?
-->


<!------------->
<!-- SECTION -->
<!------------->
## Keywords

<!-- Add keywords that fit your topic and represent the main concept. Please use only lowercase letters. -->
- ...


<!------------->
<!-- SECTION -->
<!------------->
## References

<!-- Please provide references that might be useful, e.g. links to software projects, documentation, etc. -->
- ...

